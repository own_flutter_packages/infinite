library infinite;

import 'package:flutter/material.dart';
import 'package:infinite/src/impl/infinite_impl.dart';
import 'package:infinite/src/widgets/infinite_view/infinite_view.dart';
import 'package:infinite/src/widgets/infinite_view/views/list/infinite_list_view.dart';
import 'package:infinite/src/widgets/media/infinite_view_media.dart';

import 'src/widgets/infinite_view/delegates/infinite_view_delegates.dart';

export 'src/widgets/infinite_view/delegates/infinite_view_delegates.dart'
    show InfiniteListDelegate, InfiniteGridDelegate;
export 'src/widgets/infinite_view/infinite_view.dart'
    show InfiniteViewMode, InfiniteViewState, InfiniteItemBuilder;
export 'src/widgets/infinite_view/provider/infinite_view_provider.dart';
export 'src/widgets/infinite_view/views/list/infinite_list_view.dart'
    show InfiniteItemViewData;
export 'src/widgets/media/data/infinite_media_data.dart' show InfiniteMediaData;
export 'src/widgets/media/infinite_view_media.dart' show InfiniteViewMedia;

/// Package to provide infinite views as lists, grids, maps and albums,
part 'src/infinite.dart';

/// Global constant to access Infinite widgets.
// ignore: non_constant_identifier_names
final InfiniteInterface Infinite = InfiniteImpl();
