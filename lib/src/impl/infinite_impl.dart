import 'package:flutter/material.dart';
import 'package:infinite/infinite.dart';
import 'package:infinite/src/widgets/infinite_view/infinite_view.dart';

/// Not part of public API
class InfiniteImpl implements InfiniteInterface {
  /// Not part of public API
  InfiniteImpl();

  /// Not part of public API
  @override
  InfiniteView<ItemModel, ItemData> infiniteView<ItemModel,
      ItemData extends InfiniteItemViewData<ItemModel>>({
    Key? key,
    required InfiniteViewMode mode,
    required GetData<ItemData> getData,
    required int itemsCount,
    InfiniteGridDelegate<ItemModel>? delegateGrid,
    InfiniteListDelegate<ItemModel>? delegateList,
    OnRefreshByPull? onRefresh,
    Color? colorRefreshIndicator,
    WidgetBuilder? noMoreItemsIndicatorBuilder,
    WidgetBuilder? noFoundItemsIndicatorBuilder,
  }) {
    return InfiniteView<ItemModel, ItemData>(
      key: key,
      mode: mode,
      getData: getData,
      itemsCount: itemsCount,
      delegateGrid: delegateGrid,
      delegateList: delegateList,
      onRefresh: onRefresh,
      colorRefreshIndicator: colorRefreshIndicator,
      noMoreItemsIndicatorBuilder: noMoreItemsIndicatorBuilder,
      noFoundItemsIndicatorBuilder: noFoundItemsIndicatorBuilder,
    );
  }
}
