import 'package:flutter/cupertino.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:infinite/src/services/image_service.dart';
import 'package:infinite/src/services/video_service.dart';
import 'package:uuid/uuid.dart';

class PackageLoader extends HookWidget {
  const PackageLoader({
    Key? key,
    required this.package,
  }) : super(key: key);

  final Widget package;

  @override
  Widget build(BuildContext context) {
    final reloadKey = useState(const Uuid().v1());
    useFuture(useMemoized(
      () => ImageService().init(),
      [reloadKey.value],
    ));
    useFuture(useMemoized(
      () => VideoService().init(),
      [reloadKey.value],
    ));

    return Container(
      child: package,
    );
  }
}
