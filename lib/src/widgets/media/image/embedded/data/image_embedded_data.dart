import 'package:flutter/material.dart';

class ImageEmbeddedData {
  ImageEmbeddedData({
    required this.imageUrl,
    required this.maxWidth,
    this.fit = BoxFit.contain,
    this.circle = false,
    this.backgroundColor,
    this.size,
    this.originalSize,
  });

  final String imageUrl;
  final double maxWidth;
  final BoxFit? fit;
  final bool? circle;
  final Color? backgroundColor;
  Size? size;
  Size? originalSize;
}
