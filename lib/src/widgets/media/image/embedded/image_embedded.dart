import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite/src/widgets/media/image/embedded/with_provider/provider/image_embedded_provider.dart';

class ImageEmbedded extends StatelessWidget {
  const ImageEmbedded({
    Key? key,
  }) : super(key: key);

  static Widget empty(
    double size, {
    Color? color,
  }) {
    return Container(
      color: color,
      height: size,
      width: size,
    );
  }

  @override
  Widget build(BuildContext context) {
    final provider = ImageEmbeddedProvider.of(context);

    if (provider == null) return Container();

    return ExtendedImage.network(
      provider.data.imageUrl,
      cache: true,
      imageCacheName: provider.data.imageUrl,
      enableMemoryCache: true,
      clearMemoryCacheWhenDispose: false,
      width: provider.data.size?.width,
      height: provider.data.size?.height,
      fit: _boxFit(provider),
      shape: (provider.data.circle ?? false)
          ? BoxShape.circle
          : BoxShape.rectangle,
    );
  }

  BoxFit _boxFit(ImageEmbeddedProvider provider) =>
      provider.data.fit ?? BoxFit.fill;
}
