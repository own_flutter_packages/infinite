import 'package:flutter/cupertino.dart';
import 'package:infinite/src/widgets/media/image/embedded/data/image_embedded_data.dart';
import 'package:uuid/uuid.dart';

class ImageEmbeddedProvider extends InheritedWidget {
  final String id;
  final ImageEmbeddedData data;

  ImageEmbeddedProvider({
    Key? key,
    required this.data,
    required Widget child,
  })  : id = const Uuid().v1(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(covariant ImageEmbeddedProvider oldWidget) {
    return id != oldWidget.id;
  }

  static ImageEmbeddedProvider? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<ImageEmbeddedProvider>();
  }
}
