import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:infinite/src/services/error_service.dart';
import 'package:infinite/src/services/image_service.dart';
import 'package:infinite/src/services/size_service.dart';
import 'package:infinite/src/widgets/infinite_view/provider/infinite_view_provider.dart';
import 'package:infinite/src/widgets/media/image/embedded/data/image_embedded_data.dart';
import 'package:infinite/src/widgets/media/image/embedded/image_embedded.dart';
import 'package:infinite/src/widgets/media/image/embedded/with_provider/provider/image_embedded_provider.dart';

class ImageEmbeddedWithProvider<ItemData> extends HookWidget {
  const ImageEmbeddedWithProvider({
    Key? key,
    required this.data,
  }) : super(key: key);

  final ImageEmbeddedData data;

  @override
  Widget build(BuildContext context) {
    if (data.size != null) {
      return SizedBox(
        width: data.size?.width,
        height: data.size?.height,
        child: ImageEmbeddedProvider(
          data: data,
          child: const ImageEmbedded(),
        ),
      );
    }

    final sizeProvider = InfiniteViewProvider.of<ItemData>(context);
    if (sizeProvider == null) return Container();

    final originalSize = useFuture(useMemoized(
      () => _getData(),
      [data.size],
    ));

    if (originalSize.hasData && SizeService.isSizeValid(originalSize.data)) {
      final size = SizeService.adjustHeightToWidth(
        originalSize.data!,
        data.maxWidth,
      );

      if (SizeService.isSizeValid(size)) {
        sizeProvider.add(size, data.imageUrl);

        data.size = size;

        return SizedBox(
          width: data.size?.width,
          height: data.size?.height,
          child: ImageEmbeddedProvider(
            data: data,
            child: const ImageEmbedded(),
          ),
        );
      }
    }

    return Container();
  }

  Future<Size?> _getData() async {
    if (data.imageUrl.isEmpty) return null;

    bool notFound = ImageService.isImageMarkedAsNotFound(data.imageUrl);
    if (notFound) return _onImageNotFound(data.imageUrl);

    return data.originalSize ??
        await _onCalcOriginalSize(data.imageUrl)
            .onError((error, __) => _onImageNotFound(data.imageUrl));
  }

  Future<Size?> _onCalcOriginalSize(String imageUrl) async {
    final size = await ImageService()
        .getSize(imageUrl)
        .onError(ErrorService.onErrorWithTrace);

    if (size != null) {
      return size;
    } else {
      return _onImageNotFound(imageUrl);
    }
  }

  _onImageNotFound(String imageUrl) {
    if (!ImageService.isImageMarkedAsNotFound(imageUrl)) {
      ImageService.markImageAsNotFound(imageUrl);
    }

    return null;
  }
}
