import 'package:flutter/material.dart';
import 'package:infinite/src/widgets/media/image/embedded/data/image_embedded_data.dart';
import 'package:infinite/src/widgets/media/image/embedded/with_provider/image_embedded_with_provider.dart';

class InfiniteViewImage<ItemData> extends StatelessWidget {
  const InfiniteViewImage({
    Key? key,
    required this.data,
  }) : super(key: key);

  final ImageEmbeddedData data;

  @override
  Widget build(BuildContext context) {
    return ImageEmbeddedWithProvider<ItemData>(
      data: data,
    );
  }
}
