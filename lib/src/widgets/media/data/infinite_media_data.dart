import 'dart:io' if (dart.library.html) 'dart:html';

import 'package:flutter/material.dart';

class InfiniteMediaData {
  InfiniteMediaData({
    required this.id,
    required this.url,
    this.isVideo = false,
    this.videoFile,
    this.size,
    this.visible = false,
  });

  final String id;
  final String url;
  final bool isVideo;
  final File? videoFile;
  Size? size;
  bool visible;
}
