import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:infinite/src/services/size_service.dart';
import 'package:infinite/src/widgets/infinite_view/provider/infinite_view_provider.dart';
import 'package:infinite/src/widgets/media/data/infinite_media_data.dart';
import 'package:infinite/src/widgets/media/image/embedded/data/image_embedded_data.dart';
import 'package:infinite/src/widgets/media/image/infinite_view_image.dart';
import 'package:infinite/src/widgets/media/video/embedded/data/video_embedded_data.dart';
import 'package:infinite/src/widgets/media/video/embedded/notifier/video_embedded_visibility.dart';
import 'package:uuid/uuid.dart';

import 'video/infinite_view_video.dart';

class InfiniteViewMedia<ItemData> extends HookWidget {
  const InfiniteViewMedia({
    Key? key,
    required this.data,
    required this.maxWidth,
  }) : super(key: key);

  final InfiniteMediaData data;
  final double maxWidth;

  @override
  Widget build(BuildContext context) {
    final keyReload = useState(const Uuid().v1());

    useEffect(() {
      return () => _onDispose();
    }, [keyReload.value]);

    final scrollingNotifier =
        InfiniteViewProvider.of<ItemData>(context)?.isScrollingFast();
    if (scrollingNotifier == null) return Container();

    return ValueListenableBuilder<bool>(
      valueListenable: scrollingNotifier,
      builder: (context, isFastScrolling, child) {
        if (isFastScrolling) {
          return Container();
        }

        return data.isVideo
            ? VideoEmbeddedVisibility(
                data: data,
                child: Builder(builder: (context) {
                  data.visible = _isVisible(context);

                  return InfiniteViewVideo<ItemData>(
                    data: VideoEmbeddedData(
                      id: data.id,
                      url: data.url,
                      videoFile: data.videoFile,
                      maxWidth: maxWidth,
                      fromFileSystem: false,
                      playableOnStart: false,
                      disposeWhenNotShown: true,
                      size: _widgetSize(context),
                    ),
                  );
                }),
              )
            : InfiniteViewImage<ItemData>(
                data: ImageEmbeddedData(
                  imageUrl: data.url,
                  maxWidth: maxWidth,
                  size: _widgetSize(context),
                ),
              );
      },
    );
  }

  void _onDispose() {}

  bool _isVisible(BuildContext context) {
    return VideoEmbeddedVisibility.of(context)?.notifier?.value ?? true;
  }

  Size? _widgetSize(BuildContext context) {
    final size = data.size ??
        InfiniteViewProvider.of<ItemData>(context)?.sizeOf(data.url);
    if (size == null) return null;

    return SizeService.adjustHeightToWidth(size, maxWidth);
  }
}
