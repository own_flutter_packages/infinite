import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:infinite/src/widgets/media/video/embedded/data/video_embedded_data.dart';
import 'package:infinite/src/widgets/media/video/embedded/notifier/video_embedded_visibility.dart';
import 'package:infinite/src/widgets/media/video/embedded/with_provider/video_embedded_with_provider.dart';
import 'package:uuid/uuid.dart';
import 'package:visibility_detector/visibility_detector.dart';

class InfiniteViewVideo<ItemData> extends HookWidget {
  const InfiniteViewVideo({
    Key? key,
    required this.data,
  }) : super(key: key);

  final VideoEmbeddedData data;

  @override
  Widget build(BuildContext context) {
    if (data.videoFile == null && data.url.isEmpty) return Container();

    final keyReload = useState(const Uuid().v1());

    useEffect(() {
      return () => _onDispose();
    }, [keyReload.value]);

    return SizedBox(
      width: data.size?.width,
      height: data.size?.height,
      child: _isVisible(context)
          ? VideoEmbeddedWithProvider<ItemData>(data: data)
          : EmbeddedVideoVisibilityDetector(data: data),
    );
  }

  void _onDispose() {}

  bool _isVisible(BuildContext context) {
    return VideoEmbeddedVisibility.of(context)?.notifier?.value ?? true;
  }
}

class EmbeddedVideoVisibilityDetector extends HookWidget {
  const EmbeddedVideoVisibilityDetector({
    Key? key,
    required this.data,
  }) : super(key: key);

  final VideoEmbeddedData data;

  @override
  Widget build(BuildContext context) {
    final isMounted = useIsMounted();

    return VisibilityDetector(
      key: Key('visibility_when_not_visible_${data.id}'),
      child: Container(
        constraints: const BoxConstraints(minHeight: 0.01),
      ),
      onVisibilityChanged: (info) async {
        _onVisibilityChanged(info, context, isMounted);
      },
    );
  }

  void _onVisibilityChanged(
    VisibilityInfo info,
    BuildContext context,
    IsMounted mounted,
  ) {
    if (!mounted.call()) return;

    final visibilityProvider = VideoEmbeddedVisibility.of(context);
    if (visibilityProvider == null) return;

    final visibleFraction = info.size.height > 0 ? info.visibleFraction : 0;
    visibilityProvider.notifier?.value = visibleFraction > 0.0;
  }
}
