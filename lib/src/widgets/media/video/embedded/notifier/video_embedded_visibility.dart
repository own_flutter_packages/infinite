import 'package:flutter/material.dart';
import 'package:infinite/infinite.dart';

class VideoEmbeddedVisibility extends InheritedNotifier<ValueNotifier<bool>> {
  VideoEmbeddedVisibility({
    Key? key,
    required Widget child,
    required this.data,
  }) : super(
          key: key,
          notifier: VideoEmbeddedVisibilityData(value: data.visible),
          child: child,
        );

  final InfiniteMediaData data;

  static VideoEmbeddedVisibility? of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<VideoEmbeddedVisibility>();
  }
}

class VideoEmbeddedVisibilityData extends ValueNotifier<bool> {
  VideoEmbeddedVisibilityData({bool value = false}) : super(value);

  @override
  set value(bool newValue) {
    super.value = newValue;
  }
}
