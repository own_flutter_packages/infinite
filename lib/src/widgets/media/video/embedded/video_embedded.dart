import 'dart:async';

import 'package:cached_video_player/cached_video_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:infinite/src/widgets/media/video/embedded/notifier/video_embedded_visibility.dart';
import 'package:infinite/src/widgets/media/video/embedded/with_provider/provider/video_embedded_provider.dart';
import 'package:uuid/uuid.dart';
import 'package:visibility_detector/visibility_detector.dart';

class VideoEmbedded extends HookWidget {
  const VideoEmbedded({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = VideoEmbeddedProvider.of(context);
    if (provider?.controller == null) return Container();

    if (provider!.controller.data.disposeWhenNotShown) {
      final visibilityProvider = VideoEmbeddedVisibility.of(context);
      final keyReload = useState(const Uuid().v1());
      final mounted = useIsMounted();

      useEffect(() {
        return () => _onDispose(provider);
      }, [keyReload.value]);

      return VisibilityDetector(
        key: Key('visibility_when_visible_${keyReload.value}'),
        onVisibilityChanged: (info) => _onVisibilityChanged(
          info,
          context,
          mounted.call(),
        ),
        child: SizedBox(
          width: provider.controller.data.size?.width,
          height: provider.controller.data.size?.height,
          child: (visibilityProvider?.notifier?.value ?? false) &&
                  provider.controller.videoController != null
              ? CachedVideoPlayer(provider.controller.videoController!)
              : Container(),
        ),
      );
    }

    return SizedBox(
      width: provider.controller.data.size?.width,
      height: provider.controller.data.size?.height,
      child: CachedVideoPlayer(provider.controller.videoController!),
    );
  }

  Future<void> _onVisibilityChanged(
    VisibilityInfo info,
    BuildContext context,
    bool mounted,
  ) async {
    if (!mounted) return;

    final provider = VideoEmbeddedProvider.of(context);

    if (provider?.controller == null) return;

    final visibleFraction = info.size.height > 0 ? info.visibleFraction : 0;
    final shouldShow = visibleFraction > 0.0;
    final shouldPlay = visibleFraction > 0.4;

    if (provider!.controller.playable != shouldPlay) {
      provider.controller.changeVideoPlayability(playable: shouldPlay);
    }

    if (!provider.controller.disposed && mounted) {
      final visibilityProvider = VideoEmbeddedVisibility.of(context);
      if (visibilityProvider != null) {
        visibilityProvider.notifier?.value = shouldShow;
      }
    }
  }

  void _onDispose(VideoEmbeddedProvider provider) {
    return provider.controller.dispose();
  }
}
