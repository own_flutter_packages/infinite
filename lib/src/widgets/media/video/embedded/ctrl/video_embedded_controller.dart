import 'dart:async';

import 'package:cached_video_player/cached_video_player.dart';
import 'package:infinite/src/services/error_service.dart';
import 'package:infinite/src/services/size_service.dart';
import 'package:infinite/src/services/video_service.dart';
import 'package:infinite/src/widgets/infinite_view/provider/infinite_view_provider.dart';
import 'package:infinite/src/widgets/media/video/embedded/data/video_embedded_data.dart';
import 'package:queue/queue.dart';

class VideoEmbeddedController {
  VideoEmbeddedController._({
    required this.data,
    required this.id,
  });

  VideoEmbeddedData data;
  CachedVideoPlayerController? videoController;
  bool playable = false;
  bool disposed = false;
  String id;

  static Future<VideoEmbeddedController?> of<ItemData>(
    VideoEmbeddedData data,
    InfiniteViewProvider<ItemData> viewProvider,
    Queue queue, {
    int? secondsWhenDisposed,
  }) async {
    if (secondsWhenDisposed != null) {
      data.startFrom = secondsWhenDisposed;
    }

    final controller = VideoEmbeddedController._(
      id: data.id,
      data: data,
    );

    if (!queue.isCancelled) {
      await queue.add(() => controller
          .initVideoController()
          .onError(ErrorService.onErrorWithTrace));
    }

    if (controller.data.size != null) {
      viewProvider.data.add(controller.data.size!, controller.data.url);
    }

    return controller;
  }

  Future<VideoEmbeddedController?> initVideoController({
    Queue? queue,
  }) async {
    if (data.videoFile != null) {
      videoController = CachedVideoPlayerController.file(
        data.videoFile!,
        videoPlayerOptions: VideoPlayerOptions(
          mixWithOthers: true,
        ),
      );
    } else {
      videoController = CachedVideoPlayerController.network(
        data.url,
        videoPlayerOptions: VideoPlayerOptions(
          mixWithOthers: true,
          allowBackgroundPlayback: false,
        ),
      );
    }

    if (!disposed && videoController != null) {
      if (!isVideoPlayerInitialized()) {
        try {
          videoController!.setLooping(true);

          await videoController!
              .initialize()
              .onError(ErrorService.onErrorWithTrace);

          final size = VideoService.getSizeOfVideo(videoController!);

          if (size != null && SizeService.isSizeValid(size)) {
            data.size = size;
          } else {
            ErrorService.onErrorSync('Size null when calc size of video.');
          }
        } catch (error) {
          ErrorService.onErrorSync(error, activity: 'onInitVideoController');
        }
      }

      await setVolume(0.0);
      await changeVideoPlayability(playable: data.playableOnStart);
    }

    return this;
  }

  bool isVideoPlayerInitialized() {
    if (videoController?.value == null) return false;

    return videoController!.value.isInitialized;
  }

  Future<void> setVolume(
    double volume,
  ) async {
    if (!isVideoPlayerInitialized()) return;

    await videoController!.setVolume(volume);
  }

  Future<void> changeVideoPlayability({
    bool playable = false,
    int? secondsWhenDisposed,
  }) async {
    this.playable = playable;

    if (!isVideoPlayerInitialized()) return;

    final isPlaying = videoController!.value.isPlaying;

    if (playable && !isPlaying) {
      if (secondsWhenDisposed != null || data.startFrom != null) {
        await continueFrom(secondsWhenDisposed ?? data.startFrom!);
      } else {
        await start();
      }
    } else if (!playable && isPlaying) {
      await pause();
    }
  }

  Future<void> continueFrom(int from) async {
    if (!isVideoPlayerInitialized()) return;

    await videoController!.seekTo(Duration(seconds: from));

    await videoController!.play();
  }

  Future<void> start() async {
    if (!isVideoPlayerInitialized()) return;

    await videoController!.play();
  }

  Future<void> pause() async {
    if (!isVideoPlayerInitialized()) return;

    await videoController!.pause();
  }

  void dispose() {
    disposed = true;
    if (videoController == null) return;

    data.startFrom = videoController!.value.position.inSeconds;

    try {
      videoController!.dispose();
    } catch (e) {
      ErrorService.onErrorSync(e, activity: 'onDisposeVideoController');
    }

    videoController = null;
  }
}
