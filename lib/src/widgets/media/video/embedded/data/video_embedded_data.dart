import 'dart:io';

import 'package:flutter/material.dart';

class VideoEmbeddedData {
  VideoEmbeddedData({
    required this.id,
    required this.url,
    required this.maxWidth,
    this.playableOnStart = true,
    this.disposeWhenNotShown = false,
    this.fromFileSystem = false,
    this.startFrom,
    this.videoFile,
    this.size,
  });

  String id;
  String url;
  double maxWidth;
  bool playableOnStart;
  bool disposeWhenNotShown;
  bool fromFileSystem;
  int? startFrom;
  File? videoFile;
  Size? size;
}
