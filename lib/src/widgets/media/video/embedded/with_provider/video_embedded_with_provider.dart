import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:infinite/src/widgets/infinite_view/provider/infinite_view_provider.dart';
import 'package:infinite/src/widgets/media/video/embedded/ctrl/video_embedded_controller.dart';
import 'package:infinite/src/widgets/media/video/embedded/data/video_embedded_data.dart';
import 'package:infinite/src/widgets/media/video/embedded/notifier/video_embedded_visibility.dart';
import 'package:infinite/src/widgets/media/video/embedded/video_embedded.dart';
import 'package:infinite/src/widgets/media/video/embedded/with_provider/provider/video_embedded_provider.dart';
import 'package:queue/queue.dart';
import 'package:uuid/uuid.dart';

class VideoEmbeddedWithProvider<ItemData> extends HookWidget {
  const VideoEmbeddedWithProvider({
    Key? key,
    required this.data,
  }) : super(key: key);

  final VideoEmbeddedData data;

  @override
  Widget build(BuildContext context) {
    if (!_isVisible(context)) return Container(color: Colors.deepPurple);

    final provider = InfiniteViewProvider.of<ItemData>(context);
    if (provider == null) return Container();

    final keyReload = useState(const Uuid().v1());
    final queue = useMemoized(() => Queue());
    final controller = useFuture(
      useMemoized(
          () => VideoEmbeddedController.of<ItemData>(data, provider, queue),
          [keyReload.value]),
      preserveState: true,
    );

    useEffect(() {
      return onDispose(queue, keyReload.value);
    }, [keyReload.value]);

    if (controller.connectionState != ConnectionState.done) {
      return SizedBox(
        width: data.size?.width,
        height: data.size?.height,
      );
    }
    if (controller.connectionState == ConnectionState.done &&
        !controller.hasData) {
      return Container(
        width: data.size?.width,
        height: data.size?.height,
        color: Colors.black,
      );
    }

    return SizedBox(
      width: controller.data!.data.size?.width,
      height: controller.data!.data.size?.height,
      child: VideoEmbeddedProvider(
        controller: controller.data!,
        child: const VideoEmbedded(),
      ),
    );
  }

  onDispose(Queue queue, String id) {
    if (!queue.isCancelled) {
      queue.cancel();
    }
  }

  onReload(String url) {
    data.url = url;
    // this.url = url;
    // videoFile = null;
    // future = _getData();
    // if (mounted) {
    //   setState(() {});
    // }
  }

  bool _isVisible(BuildContext context) {
    return VideoEmbeddedVisibility.of(context)?.notifier?.value ?? true;
  }
}
