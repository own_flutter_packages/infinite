import 'package:flutter/cupertino.dart';
import 'package:infinite/src/widgets/media/video/embedded/ctrl/video_embedded_controller.dart';
import 'package:uuid/uuid.dart';

class VideoEmbeddedProvider extends InheritedWidget {
  final String id;
  final VideoEmbeddedController controller;

  VideoEmbeddedProvider({
    Key? key,
    required this.controller,
    required Widget child,
  })  : id = const Uuid().v1(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(covariant VideoEmbeddedProvider oldWidget) {
    return id != oldWidget.id;
  }

  static VideoEmbeddedProvider? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<VideoEmbeddedProvider>();
  }
}
