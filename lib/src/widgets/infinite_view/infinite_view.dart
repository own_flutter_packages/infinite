import 'package:flutter/material.dart';
import 'package:infinite/src/widgets/infinite_view/controller/infinite_view_controller.dart';
import 'package:infinite/src/widgets/infinite_view/delegates/infinite_view_delegates.dart';
import 'package:infinite/src/widgets/infinite_view/provider/infinite_view_provider.dart';
import 'package:infinite/src/widgets/infinite_view/views/embedded/infinite_view_embedded.dart';
import 'package:infinite/src/widgets/infinite_view/views/list/infinite_list_view.dart';
import 'package:infinite/src/widgets/media/infinite_view_media.dart';
import 'package:infinite/src/widgets/package_loader/package_loader.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

typedef GetData<ItemModel> = Future<List<ItemModel>> Function(
  int startIndex,
  int nextItemsCount,
);

typedef InfiniteItemBuilder<ItemModel> = Widget Function(
  BuildContext context,
  ItemModel data,
  int index,
);

typedef InfiniteMediaItemBuilder<ItemModel> = Widget Function(
  BuildContext context,
  ItemModel data,
  int index,
  List<InfiniteViewMedia> media,
);

typedef OnRefreshByPull = Future<void> Function();

/// Root widget that combines [InfiniteViewMode.grid] and [InfiniteViewMode.list].
///
/// Since the widget is stateful you can provide a [key] and use these functions:
/// - [onReloadMode]
/// - [clear]
///
/// When reloading the mode the values of loaded items are passed to the other view.
///
/// You have to specify [getData] to be able to fetch items.
/// Of course you can extend its usage by a custom init function for each item.
///
/// '''dart
///  Future<List<InfiniteItemViewData<MyListData>>> _onGetData(
///    startIndex,
///     nextItemsCount,
///   ) async {
///     if (startIndex < 0) return [];
///
///     int endIndex = startIndex + nextItemsCount + 1;
///     endIndex = endIndex < items.length ? endIndex : items.length;
///
///     final items = await getItems();
///     return __onInitItems(items);
///   }
/// '''
///
/// The property [itemsCount] is required since the list should know when last page is reached.
/// Until the last page all items will be fetched when last item is reached on scroll.
///
/// If you use [InfiniteViewMode.grid] you have to provide a [InfiniteGridDelegate].
/// If you use [InfiniteViewMode.list] you have to provide a [InfiniteListDelegate].
///
/// In delegate you have the possibility to provide a [builder] or [builderWithMedia].
/// When [builderWithMedia] is provided a list of [InfiniteViewMedia] is returned.
/// You will have to provide the appropriate [InfiniteMediaData] when you provide [InfiniteItemViewData] during startup.
/// You can is it in your widget tree as in following example.
///
/// ```dart
/// builderWithMedia: (context, value, index, media) => Container(
///     color: value.color,
///     child: Stack(
///       children: [
///         media.isNotEmpty ? media.first : Container(),
///         Align(
///           alignment: Alignment.bottomCenter,
///           child: Text(value.text),
///         ),
///       ],
///     ),
///   ),
/// ```
///
/// Use [onRefresh] for custom callback when user pulls the list down.
/// Use [colorRefreshIndicator] for custom color for indicator when user pulls the list down.
/// Use [noMoreItemsIndicatorBuilder] for custom view when user scrolled to the end.
/// Use [noFoundItemsIndicatorBuilder] for custom view when no items were presented.
class InfiniteView<ItemModel, ItemData extends InfiniteItemViewData<ItemModel>>
    extends StatefulWidget {
  const InfiniteView({
    Key? key,
    required this.mode,
    required this.getData,
    required this.itemsCount,
    this.delegateGrid,
    this.delegateList,
    this.onRefresh,
    this.colorRefreshIndicator,
    this.noMoreItemsIndicatorBuilder,
    this.noFoundItemsIndicatorBuilder,
  })  : assert(delegateGrid != null || delegateList != null),
        super(key: key);

  final InfiniteViewMode mode;
  final GetData<ItemData> getData;
  final int itemsCount;
  final InfiniteGridDelegate<ItemModel>? delegateGrid;
  final InfiniteListDelegate<ItemModel>? delegateList;
  final OnRefreshByPull? onRefresh;
  final Color? colorRefreshIndicator;
  final WidgetBuilder? noMoreItemsIndicatorBuilder;
  final WidgetBuilder? noFoundItemsIndicatorBuilder;

  @override
  InfiniteViewState<ItemModel, ItemData> createState() =>
      InfiniteViewState<ItemModel, ItemData>();
}

class InfiniteViewState<ItemModel,
        ItemData extends InfiniteItemViewData<ItemModel>>
    extends State<InfiniteView<ItemModel, ItemData>> {
  InfiniteViewMode get mode => _controller.mode;

  late InfiniteViewController<ItemData> _controller;

  /// Reloads the mode of view.
  ///
  /// When view is changed all items are passed to [InfiniteViewController] of other view.
  onReloadMode(InfiniteViewMode mode) {
    if (_controller.mode != mode && mounted) {
      setState(() {
        _controller.mode = mode;
      });
    }
  }

  /// Initializes controller during state init.
  ///
  /// [InfiniteViewController] provides the [PagingController] and [InfiniteViewMode] to widget tree by [InfiniteViewProvider].
  @override
  void initState() {
    _controller = InfiniteViewController<ItemData>(
      pagingController: PagingController<int, ItemData>(
        firstPageKey: 0,
        invisibleItemsThreshold: 1,
      ),
      mode: widget.mode,
    );

    super.initState();
  }

  /// Disposes [PagingController] and clears list of media sizes in [InfiniteViewController].
  @override
  void dispose() {
    _controller.clear();
    _controller.pagingController?.dispose();
    super.dispose();
  }

  /// When building [InfiniteViewEmbedded] in [PackageLoader] required services [ImageService] and [VideoService] are inited.
  ///
  /// Creates [InfiniteViewProvider] that is passed down the tree.
  /// It provides [InfiniteViewController] which holds [PagingController] and [InfiniteViewMode]
  @override
  Widget build(BuildContext context) {
    return PackageLoader(
      package: InfiniteViewProvider<ItemData>(
        data: _controller,
        child: InfiniteViewEmbedded<ItemModel, ItemData>(
          getData: widget.getData,
          itemsCount: widget.itemsCount,
          delegateGrid: widget.delegateGrid,
          delegateList: widget.delegateList,
          onRefresh: widget.onRefresh,
          colorRefreshIndicator: widget.colorRefreshIndicator,
          noMoreItemsIndicatorBuilder: widget.noMoreItemsIndicatorBuilder,
          noFoundItemsIndicatorBuilder: widget.noFoundItemsIndicatorBuilder,
        ),
      ),
    );
  }

  /// Clears cache of [PagingController] (including of all items data) and clears list of media sizes in [InfiniteViewController].
  clear() {
    _controller.pagingController?.refresh();
    _controller.clear();
  }
}

/// Required to separate different view modes.
enum InfiniteViewMode { list, grid }
