import 'package:infinite/src/widgets/infinite_view/infinite_view.dart';

class InfiniteGridDelegate<ItemModel> {
  const InfiniteGridDelegate({
    required this.itemExtent,
    required this.maxWidth,
    this.builder,
    this.builderWithMedia,
    this.columnsCount,
    this.pageSize,
  }) : assert(builder != null || builderWithMedia != null);

  final double itemExtent;
  final double maxWidth;
  final InfiniteItemBuilder<ItemModel>? builder;
  final InfiniteMediaItemBuilder<ItemModel>? builderWithMedia;
  final int? columnsCount;
  final int? pageSize;
}

class InfiniteListDelegate<ItemModel> {
  const InfiniteListDelegate({
    required this.itemExtent,
    required this.maxWidth,
    this.builder,
    this.builderWithMedia,
    this.pageSize,
  }) : assert(builder != null || builderWithMedia != null);

  final double itemExtent;
  final double maxWidth;
  final InfiniteItemBuilder<ItemModel>? builder;
  final InfiniteMediaItemBuilder<ItemModel>? builderWithMedia;
  final int? pageSize;
}
