import 'dart:async';

import 'package:flutter/material.dart';
import 'package:infinite/src/services/error_service.dart';
import 'package:infinite/src/widgets/infinite_view/delegates/infinite_view_delegates.dart';
import 'package:infinite/src/widgets/infinite_view/infinite_view.dart';
import 'package:infinite/src/widgets/infinite_view/provider/infinite_view_provider.dart';
import 'package:infinite/src/widgets/infinite_view/views/list/infinite_list_view.dart';
import 'package:infinite/src/widgets/media/infinite_view_media.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class InfiniteGridView<ItemModel,
    ItemData extends InfiniteItemViewData<ItemModel>> extends StatefulWidget {
  static const int columnsCountDefault = 3;
  static const int pageSizeDefault = 15;

  const InfiniteGridView({
    Key? key,
    required this.getData,
    required this.delegate,
    required this.itemsCount,
    this.noMoreItemsIndicatorBuilder,
    this.noFoundItemsIndicatorBuilder,
  }) : super(key: key);

  final GetData<ItemData> getData;
  final InfiniteGridDelegate<ItemModel> delegate;
  final int itemsCount;
  final WidgetBuilder? noMoreItemsIndicatorBuilder;
  final WidgetBuilder? noFoundItemsIndicatorBuilder;

  @override
  InfiniteGridViewState<ItemModel, ItemData> createState() =>
      InfiniteGridViewState<ItemModel, ItemData>();
}

class InfiniteGridViewState<ItemModel,
        ItemData extends InfiniteItemViewData<ItemModel>>
    extends State<InfiniteGridView<ItemModel, ItemData>> {
  late PagingController<int, ItemData> _pagingController;
  late ScrollController _scrollController;

  late int itemsCount;
  late int pageSize;

  int? pageIndex;
  bool isControllerInherited = false;

  Future<void> _fetchPage(int pageKey) async {
    if (pageSize == 0) return;

    pageIndex = pageKey ~/ pageSize;

    try {
      final items = await widget
          .getData(pageKey, pageSize)
          .onError(ErrorService.onErrorWithTraceIterable);

      final isLastPage = pageKey >= (itemsCount - pageSize);

      if (isLastPage) {
        _pagingController.appendLastPage(items);
      } else {
        final int nextPageKey = pageKey + items.length;
        _pagingController.appendPage(items, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  void initState() {
    itemsCount = widget.itemsCount;
    pageSize = widget.delegate.pageSize ?? InfiniteGridView.pageSizeDefault;

    _scrollController = ScrollController();
    _scrollController.addListener(_onScrolled);

    _onInitController();

    super.initState();
  }

  @override
  void dispose() {
    _pagingController.removePageRequestListener(_fetchPage);
    _scrollController.removeListener(_onScrolled);
    _scrollController.dispose();

    if (!isControllerInherited) {
      _pagingController.dispose();
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _onInitController(context: context);

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _scrollController.position.isScrollingNotifier.addListener(
        _onScrollInNotifier,
      );
    });
    return PagedGridView<int, ItemData>(
      pagingController: _pagingController,
      scrollController: _scrollController,
      physics: const AlwaysScrollableScrollPhysics(),
      cacheExtent: _cacheExtent(),
      addAutomaticKeepAlives: false,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: _crossAxisExtent(),
        mainAxisExtent: _mainAxisExtent(),
      ),
      builderDelegate: PagedChildBuilderDelegate<ItemData>(
        itemBuilder: _builder,
        noMoreItemsIndicatorBuilder: widget.noMoreItemsIndicatorBuilder,
        noItemsFoundIndicatorBuilder: widget.noFoundItemsIndicatorBuilder,
        firstPageProgressIndicatorBuilder: (_) => Container(),
        newPageProgressIndicatorBuilder: (_) => Container(),
      ),
    );
  }

  Widget _builder(BuildContext context, ItemData item, int index) {
    if (widget.delegate.builderWithMedia != null && item.media != null) {
      final columnsCount =
          widget.delegate.columnsCount ?? InfiniteGridView.columnsCountDefault;

      final media = item.media
              ?.map((e) => InfiniteViewMedia<ItemData>(
                    data: e,
                    maxWidth: widget.delegate.maxWidth / columnsCount,
                  ))
              .toList() ??
          [];

      return widget.delegate.builderWithMedia!(
        context,
        item.value,
        index,
        media,
      );
    }
    return widget.delegate.builder!(context, item.value, index);
  }

  void _onInitController({BuildContext? context}) {
    if (isControllerInherited) return;

    isControllerInherited = false;

    if (context != null) {
      final provider = InfiniteViewProvider.of<ItemData>(context);

      if (provider?.data.pagingController != null) {
        _pagingController = provider!.data.pagingController!;
        isControllerInherited = true;
      }
    } else {
      _pagingController = PagingController<int, ItemData>(
        firstPageKey: 0,
        invisibleItemsThreshold: 1,
      );
    }

    _pagingController.removePageRequestListener(_fetchPage);
    _pagingController.addPageRequestListener(_fetchPage);
  }

  void _onScrolled() {
    final velocity = _scrollController.position.activity?.velocity;

    if (velocity == null) return;

    if (velocity > 7000) {
      PaintingBinding.instance.imageCache.clear();
      PaintingBinding.instance.imageCache.clearLiveImages();
    }

    final controller = InfiniteViewProvider.of<ItemData>(context);
    if (controller == null) return;

    if (velocity > 2000) {
      controller.setFastScrolling(true);
    }

    if (velocity <= 2000) {
      controller.setFastScrolling(false);
    }
  }

  void _onScrollInNotifier() {
    if (!_scrollController.position.isScrollingNotifier.value) {
      final controller = InfiniteViewProvider.of<ItemData>(context);
      if (controller == null) return;

      controller.setFastScrolling(false);
    }
  }

  int _crossAxisExtent() =>
      widget.delegate.columnsCount ?? InfiniteGridView.columnsCountDefault;

  double _mainAxisExtent() => widget.delegate.itemExtent;

  double? _cacheExtent() {
    const extent = 1000.0;

    return extent;
  }
}
