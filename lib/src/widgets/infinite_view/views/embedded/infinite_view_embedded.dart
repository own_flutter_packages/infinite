import 'package:flutter/material.dart';
import 'package:infinite/infinite.dart';
import 'package:infinite/src/widgets/infinite_view/infinite_view.dart';
import 'package:infinite/src/widgets/infinite_view/views/grid/infinite_grid_view.dart';
import 'package:infinite/src/widgets/infinite_view/views/list/infinite_list_view.dart';

class InfiniteViewEmbedded<ItemModel,
    ItemData extends InfiniteItemViewData<ItemModel>> extends StatelessWidget {
  const InfiniteViewEmbedded({
    Key? key,
    required this.getData,
    required this.itemsCount,
    this.delegateGrid,
    this.delegateList,
    this.onRefresh,
    this.colorRefreshIndicator,
    this.noMoreItemsIndicatorBuilder,
    this.noFoundItemsIndicatorBuilder,
  }) : super(key: key);

  final GetData<ItemData> getData;
  final int itemsCount;
  final InfiniteGridDelegate<ItemModel>? delegateGrid;
  final InfiniteListDelegate<ItemModel>? delegateList;
  final OnRefreshByPull? onRefresh;
  final Color? colorRefreshIndicator;
  final WidgetBuilder? noMoreItemsIndicatorBuilder;
  final WidgetBuilder? noFoundItemsIndicatorBuilder;

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: _onRefreshByPull,
      color: colorRefreshIndicator,
      backgroundColor: Colors.transparent,
      child: _isList(context)
          ? InfiniteListView<ItemModel, ItemData>(
              getData: getData,
              delegate: delegateList!,
              itemsCount: itemsCount,
              noMoreItemsIndicatorBuilder: noMoreItemsIndicatorBuilder,
              noFoundItemsIndicatorBuilder: noFoundItemsIndicatorBuilder,
            )
          : _isGrid(context)
              ? InfiniteGridView<ItemModel, ItemData>(
                  getData: getData,
                  delegate: delegateGrid!,
                  itemsCount: itemsCount,
                  noMoreItemsIndicatorBuilder: noMoreItemsIndicatorBuilder,
                  noFoundItemsIndicatorBuilder: noFoundItemsIndicatorBuilder,
                )
              : Container(),
    );
  }

  Future<void> _onRefreshByPull() async {
    if (onRefresh != null) {
      await onRefresh!();
    }
  }

  bool _isGrid(BuildContext context) {
    final provider = InfiniteViewProvider.of<ItemData>(context);
    if (provider == null) return false;
    return provider.data.mode == InfiniteViewMode.grid && delegateGrid != null;
  }

  bool _isList(BuildContext context) {
    final provider = InfiniteViewProvider.of<ItemData>(context);
    if (provider == null) return false;

    return provider.data.mode == InfiniteViewMode.list && delegateList != null;
  }
}
