import 'dart:async';

import 'package:flutter/material.dart';
import 'package:infinite/src/services/error_service.dart';
import 'package:infinite/src/widgets/infinite_view/delegates/infinite_view_delegates.dart';
import 'package:infinite/src/widgets/infinite_view/infinite_view.dart';
import 'package:infinite/src/widgets/infinite_view/provider/infinite_view_provider.dart';
import 'package:infinite/src/widgets/media/data/infinite_media_data.dart';
import 'package:infinite/src/widgets/media/infinite_view_media.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class InfiniteItemViewData<ItemModel> {
  const InfiniteItemViewData({
    required this.value,
    this.media,
  });

  final ItemModel value;
  final List<InfiniteMediaData>? media;
}

class InfiniteListView<ItemModel,
    ItemData extends InfiniteItemViewData<ItemModel>> extends StatefulWidget {
  static const int pageSizeDefault = 7;

  const InfiniteListView({
    Key? key,
    required this.getData,
    required this.delegate,
    required this.itemsCount,
    this.noMoreItemsIndicatorBuilder,
    this.noFoundItemsIndicatorBuilder,
  }) : super(key: key);

  final GetData<ItemData> getData;
  final InfiniteListDelegate<ItemModel> delegate;
  final int itemsCount;
  final WidgetBuilder? noMoreItemsIndicatorBuilder;
  final WidgetBuilder? noFoundItemsIndicatorBuilder;

  @override
  InfiniteListViewState<ItemModel, ItemData> createState() =>
      InfiniteListViewState<ItemModel, ItemData>();
}

class InfiniteListViewState<ItemModel,
        ItemData extends InfiniteItemViewData<ItemModel>>
    extends State<InfiniteListView<ItemModel, ItemData>> {
  late PagingController<int, ItemData> _pagingController;
  late int pageSize;

  int? pageIndex;
  bool isControllerInherited = false;

  Future<void> _fetchPage(int pageKey) async {
    pageIndex = pageKey ~/ pageSize;

    try {
      final items = await widget
          .getData(pageKey, pageSize)
          .onError(ErrorService.onErrorWithTraceIterable);

      final isLastPage = pageKey >= (widget.itemsCount - pageSize);

      if (isLastPage) {
        _pagingController.appendLastPage(items);
      } else {
        final int nextPageKey = pageKey + items.length;
        _pagingController.appendPage(items, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  void initState() {
    pageSize = widget.delegate.pageSize ?? InfiniteListView.pageSizeDefault;

    super.initState();
  }

  @override
  void dispose() {
    _pagingController.removePageRequestListener(_fetchPage);

    if (!isControllerInherited) {
      _pagingController.dispose();
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final provider = InfiniteViewProvider.of<ItemData>(context);
    _onInitController(provider!);

    return PagedListView<int, ItemData>(
      pagingController: _pagingController,
      physics: const AlwaysScrollableScrollPhysics(),
      cacheExtent: _cacheExtent(),
      addAutomaticKeepAlives: false,
      builderDelegate: PagedChildBuilderDelegate<ItemData>(
        itemBuilder: (context, item, index) {
          if (widget.delegate.builderWithMedia != null && item.media != null) {
            final media = item.media
                    ?.map((e) => InfiniteViewMedia<ItemData>(
                          data: e,
                          maxWidth: widget.delegate.maxWidth,
                        ))
                    .toList() ??
                [];

            return widget.delegate.builderWithMedia!(
              context,
              item.value,
              index,
              media,
            );
          }
          return widget.delegate.builder!(context, item.value, index);
        },
        noMoreItemsIndicatorBuilder: widget.noMoreItemsIndicatorBuilder,
        noItemsFoundIndicatorBuilder: widget.noFoundItemsIndicatorBuilder,
        firstPageProgressIndicatorBuilder: (_) => Container(),
        newPageProgressIndicatorBuilder: (_) => Container(),
      ),
    );
  }

  void _onInitController(InfiniteViewProvider<ItemData> provider) {
    isControllerInherited = false;

    if (provider.data.pagingController != null) {
      _pagingController = provider.data.pagingController!;
      isControllerInherited = true;
    }

    if (!isControllerInherited) {
      _pagingController = PagingController<int, ItemData>(
        firstPageKey: 0,
        invisibleItemsThreshold: 1,
      );
    }

    _pagingController.removePageRequestListener(_fetchPage);
    _pagingController.addPageRequestListener(_fetchPage);
  }

  double? _cacheExtent() {
    final extent = widget.delegate.itemExtent * 0.25 * pageSize;

    return extent;
  }
}
