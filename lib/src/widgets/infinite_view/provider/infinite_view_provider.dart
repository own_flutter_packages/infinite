import 'package:flutter/material.dart';
import 'package:infinite/src/widgets/infinite_view/controller/infinite_view_controller.dart';
import 'package:uuid/uuid.dart';

class InfiniteViewProvider<ItemData> extends InheritedWidget {
  InfiniteViewProvider({
    Key? key,
    required this.data,
    required Widget child,
  })  : id = const Uuid().v1(),
        super(
          key: key,
          child: child,
        );

  final String id;
  final InfiniteViewController<ItemData> data;
  final FastScrollNotifier _isScrollingFast = FastScrollNotifier(false);

  void setFastScrolling(bool isScrollingFast) {
    _isScrollingFast.value = isScrollingFast;
  }

  void add(Size size, String url) {
    data.add(size, url);
  }

  FastScrollNotifier isScrollingFast() {
    return _isScrollingFast;
  }

  Size? sizeOf(String url) {
    return data.sizeOf(url);
  }

  void clear() {
    data.clear();
  }

  @override
  bool updateShouldNotify(
    covariant InfiniteViewProvider<ItemData> oldWidget,
  ) {
    return id != oldWidget.id;
  }

  static InfiniteViewProvider<ItemData>? of<ItemData>(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<InfiniteViewProvider<ItemData>>();
  }
}

class FastScrollNotifier extends ValueNotifier<bool> {
  FastScrollNotifier(bool value) : super(value);
}
