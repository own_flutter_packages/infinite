import 'package:flutter/material.dart';
import 'package:infinite/src/widgets/infinite_view/infinite_view.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class InfiniteViewController<ItemData> {
  InfiniteViewController({
    required this.mode,
    required this.pagingController,
  });

  PagingController<int, ItemData>? pagingController;
  InfiniteViewMode mode;

  final Map<String, Size> _mediaSizes = {};

  void add(Size size, String url) {
    if (!_mediaSizes.containsKey(url)) {
      _mediaSizes.putIfAbsent(url, () => size);
    }
  }

  Size? sizeOf(String url) {
    if (_mediaSizes.containsKey(url)) {
      return _mediaSizes[url];
    }

    return null;
  }

  void clear() {
    _mediaSizes.clear();
  }
}
