import 'dart:async';

import 'package:cached_video_player/cached_video_player.dart';
import 'package:flutter/material.dart';
import 'package:infinite/src/services/error_service.dart';
import 'package:queue/queue.dart';
import 'package:visibility_detector/visibility_detector.dart';

/// Provides functionality for videos.
class VideoService {
  static final VideoService _singleton = VideoService._internal();

  factory VideoService() {
    return _singleton;
  }

  Queue? queue;

  VideoService._internal();

  /// Init service.
  ///
  /// If before is provided it is executed before and after init.
  Future<VideoService> init({Function(int)? before}) async {
    if (before != null) {
      before(-1);
    }

    await Future.delayed(const Duration(milliseconds: 0));

    queue ??= Queue(delay: const Duration(milliseconds: 50));

    VisibilityDetectorController().updateInterval = const Duration(
      milliseconds: 100,
    );

    if (before != null) {
      before(1);
    }

    return _singleton;
  }

  /// Return size of video by access of video player controller.
  static Size? getSizeOfVideo(CachedVideoPlayerController ctrl) {
    try {
      final value = ctrl.value;
      final videoWidth = value.size.width;
      final videoHeight = value.size.height;

      return Size(videoWidth, videoHeight);
    } catch (e) {
      return ErrorService.onErrorSync(e, activity: 'getSizeOfVideo');
    }
  }
}
