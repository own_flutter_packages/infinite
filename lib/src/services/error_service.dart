import 'dart:async';

import 'package:flutter/foundation.dart';

/// Provides functionality for errors.
class ErrorService {
  static final ErrorService _singleton = ErrorService._internal();

  factory ErrorService() {
    return _singleton;
  }

  ErrorService._internal();

  /// Init service.
  ///
  /// If before is provided it is executed before and after init.
  Future<ErrorService> init({Function(int)? before}) async {
    if (before != null) {
      before(-1);
    }

    await Future.delayed(const Duration(milliseconds: 0));
    if (before != null) {
      before(1);
    }

    return _singleton;
  }

  /// Handle async error with stack trace.
  static Future<T?> onErrorWithTrace<T>(
    dynamic error,
    dynamic stacktrace,
  ) async {
    onPrintError(error);
    return null;
  }

  /// Handle async error with stack trace and return type Integer.
  static Future<int> onErrorWithTraceInteger<T>(
    dynamic error,
    dynamic stacktrace,
  ) async {
    onPrintError(error);
    return -1;
  }

  /// Handle async error with stack trace and return type Bool.
  static Future<bool> onErrorWithTraceBool<T>(
    dynamic error,
    dynamic stacktrace,
  ) async {
    onPrintError(error);
    return false;
  }

  /// Handle async error with stack trace and return type Iterable.
  static Future<List<S>> onErrorWithTraceIterable<S, T extends List<S>>(
    dynamic error,
    dynamic stacktrace,
  ) async {
    onPrintError(error);
    return <S>[];
  }

  /// Handle async error with return type.
  static Future<T?> onError<T>(
    dynamic error, {
    String? activity,
  }) async {
    onPrintError(error, activity: activity);
    return null;
  }

  /// Handle sync error with return type.
  static T? onErrorSync<T>(
    dynamic error, {
    String? activity,
  }) {
    onPrintError(error, activity: activity);
    return null;
  }

  /// Prints error to console.
  ///
  /// In future this will be replaced by logger.
  static void onPrintError(
    error, {
    String? activity,
  }) {
    if (error == null) {
      if (kDebugMode) {
        print('[ERROR] | $activity | when no error message available.');
      }
      return;
    }

    if (error is String) {
      if (kDebugMode) {
        print('[ERROR] | $activity | when Error: $error.');
      }
      return;
    }

    if (error is Error) {
      if (kDebugMode) {
        print('[ERROR] | $activity | when Error: ${error.toString()}.');
      }
      return;
    }

    if (kDebugMode) {
      print('[ERROR] | $activity | when unknown error.');
    }
  }
}
