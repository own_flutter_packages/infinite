import 'dart:async';

import 'package:flutter/material.dart';
import 'package:infinite/src/services/error_service.dart';

/// Provides functionality in size issues.
class SizeService {
  static final SizeService _singleton = SizeService._internal();

  factory SizeService() {
    return _singleton;
  }

  SizeService._internal();

  /// Init service.
  ///
  /// If before is provided it is executed before and after init.
  Future<SizeService> init({Function(int)? before}) async {
    if (before != null) {
      before(-1);
    }

    await Future.delayed(const Duration(milliseconds: 0));

    if (before != null) {
      before(1);
    }

    return _singleton;
  }

  /// Returns valid size from media adjusted for storage in memory.
  static Size? getDecodedSize(
    double screenWidth, {
    Size? visibleSize,
    Size? originalSize,
  }) {
    int? decodeWidth;
    int? decodeHeight;

    double? visibleWidth = visibleSize?.width;
    double? visibleHeight = visibleSize?.height;

    if (originalSize != null) {
      try {
        final int deltaWidth = visibleWidth != null && visibleWidth != 0
            ? originalSize.width ~/ visibleWidth
            : 0;
        final int deltaHeight = visibleHeight != null && visibleHeight != 0
            ? originalSize.height ~/ visibleHeight
            : 0;

        final int delta = deltaWidth > deltaHeight ? deltaWidth : deltaHeight;

        final double? screenDelta = visibleWidth != null && visibleWidth != 0
            ? screenWidth / visibleWidth
            : null;
        final double? factor = screenDelta != null
            ? (1 / delta) *
                2.5 *
                (screenDelta > 1.1 ? 0.5 + (1 / screenDelta) : 1)
            : null;

        if ((delta > 2)) {
          if (factor != null) {
            decodeWidth = (originalSize.width * factor).toInt();
          }

          if (factor != null) {
            decodeHeight = (originalSize.height * factor).toInt();
          }
        } else {
          decodeWidth = originalSize.width.toInt();
          decodeHeight = originalSize.height.toInt();
        }
      } catch (e) {
        return ErrorService.onErrorSync(e, activity: 'getDecodedSize');
      }
    }

    if (decodeWidth != null && decodeHeight != null) {
      return Size(decodeWidth.toDouble(), decodeHeight.toDouble());
    }

    return null;
  }

  /// Adjusts height to width.
  static Size adjustHeightToWidth(Size size, double width) {
    if (!isSizeValid(size)) return size;

    if (size.height != 0) {
      try {
        final ratio = size.width / size.height;

        final double? height = ratio != 0 ? width / ratio : null;

        return height != null ? Size(width, height) : size;
      } catch (e) {
        return ErrorService.onErrorSync(e, activity: 'adjustHeightToWidth');
      }
    }

    return size;
  }

  /// Returns true if size is valid.
  static bool isSizeValid(Size? size) {
    if (size == null) return false;

    if (!isSizeValueValid(size.width)) return false;
    if (!isSizeValueValid(size.height)) return false;

    return true;
  }

  /// Returns true if size value is valid.
  static bool isSizeValueValid(double size) {
    return !size.isNaN && !size.isInfinite && !size.isNegative;
  }
}
