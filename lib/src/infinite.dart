part of infinite;

/// The main API interface of Infinite. Available through the `Infinite` constant.
abstract class InfiniteInterface {
  /// Widget of infinite view of types [InfiniteViewMode.grid] or [InfiniteViewMode.list].
  ///
  /// Since the widget is stateful you can provide a [key] and use these functions:
  /// - [onReloadMode]
  /// - [clear]
  ///
  /// You have to specify [getData] to be able to fetch items.
  /// Of course you can extend its usage by a custom init function for each item.
  ///
  /// '''dart
  ///  Future<List<InfiniteItemViewData<MyListData>>> _onGetData(
  ///    startIndex,
  ///     nextItemsCount,
  ///   ) async {
  ///     if (startIndex < 0) return [];
  ///
  ///     int endIndex = startIndex + nextItemsCount + 1;
  ///     endIndex = endIndex < items.length ? endIndex : items.length;
  ///
  ///     final items = await getItems();
  ///     return _onInitItems(items);
  ///   }
  /// '''
  ///
  /// The property [itemsCount] is required since the list should know when last page is reached.
  /// Until the last page all items will be fetched when last item is reached on scroll.
  ///
  /// If you use [InfiniteViewMode.grid] you have to provide a [InfiniteGridDelegate].
  /// If you use [InfiniteViewMode.list] you have to provide a [InfiniteListDelegate].
  ///
  /// In delegate you have the possibility to provide a [builder] or [builderWithMedia].
  /// When [builderWithMedia] is provided a list of [InfiniteViewMedia] is returned.
  /// You will have to provide the appropriate [InfiniteMediaData] when you provide [InfiniteItemViewData] during startup.
  /// You can is it in your widget tree as in following example.
  ///
  /// ```dart
  /// builderWithMedia: (context, value, index, media) => Container(
  ///     color: value.color,
  ///     child: Stack(
  ///       children: [
  ///         media.isNotEmpty ? media.first : Container(),
  ///         Align(
  ///           alignment: Alignment.bottomCenter,
  ///           child: Text(value.text),
  ///         ),
  ///       ],
  ///     ),
  ///   ),
  /// ```
  ///
  /// Use [onRefresh] for custom callback when user pulls the list down.
  /// Use [colorRefreshIndicator] for custom color for indicator when user pulls the list down.
  /// Use [noMoreItemsIndicatorBuilder] for custom view when user scrolled to the end.
  /// Use [noFoundItemsIndicatorBuilder] for custom view when no items were presented.
  InfiniteView<ItemModel, ItemData> infiniteView<ItemModel,
      ItemData extends InfiniteItemViewData<ItemModel>>({
    Key? key,
    required InfiniteViewMode mode,
    required GetData<ItemData> getData,
    required int itemsCount,
    InfiniteGridDelegate<ItemModel>? delegateGrid,
    InfiniteListDelegate<ItemModel>? delegateList,
    OnRefreshByPull? onRefresh,
    Color? colorRefreshIndicator,
    WidgetBuilder? noMoreItemsIndicatorBuilder,
    WidgetBuilder? noFoundItemsIndicatorBuilder,
  });
}
