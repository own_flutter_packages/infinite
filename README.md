Package with infinite view of list and grid. It is optimized to hold big data and expecially for rendering high amount of media as images and videos.


## Source code

Source code is available on [GitLab](https://gitlab.com/own_flutter_packages/infinite).

## Features

- You can choose between ``InfiniteViewMode.grid`` or ``InfiniteViewMode.list``.
- You are able to switch the selected mode
- When mode is switched all fetched data is passed to new mode
- No need to write own ``Image`` or ``Video`` widgets
- High load of media will keep your app at high performance.

## Getting started

Add to ```pubspec.yaml```
```dart
dependencies:
  infinite: ^1.0.0
```

Then in the console of your project folder
```dart
flutter pub get
```

Prerequisites:
```dart
environment:
  sdk: ">=2.12.0 <3.0.0"
```

## Usage
Widget of infinite view of types ``InfiniteViewMode.grid`` or ``InfiniteViewMode.list``.

Since the widget is stateful you can provide a ``key`` and use these functions:

```dart
reloadMode() {
  key.currentState.onReloadMode(InfiniteViewMode.list); 
}
clear() {
  key.currentState.clear(); 
}
```

You have to specify ``getData`` to be able to fetch items.
Of course you can extend its usage by a custom init function for each item.

```dart
 Future<List<InfiniteItemViewData<MyListData>>> _onGetData(
   startIndex,
    nextItemsCount,
  ) async {
    if (startIndex < 0) return [];

    int endIndex = startIndex + nextItemsCount + 1;
    endIndex = endIndex < items.length ? endIndex : items.length;

    final items = await getItems();
    return _onInitItems(items);
  }
```

The property ``itemsCount`` is required since the list should know when last page is reached.
Until the last page all items will be fetched when last item is reached on scroll.

If you use ``InfiniteViewMode.grid`` you have to provide a ``InfiniteGridDelegate``.
If you use ``InfiniteViewMode.list`` you have to provide a ``InfiniteListDelegate``.

### Delegate
```dart
delegateGrid: InfiniteGridDelegate(
      maxWidth: size.width,
      builder: (context, value, index) => Container(
        color: value.color,
        child: Text(value.text),
      ),
      itemExtent: size.width / 2,
      columnsCount: 3,
      pageSize: 20,
    ),
    getData: _onGetData,
    onRefresh: _onRefresh,
  ),
```

In delegate you have the possibility to provide a ``builder`` or ``builderWithMedia``.
When ``builderWithMedia`` is provided a list of ``InfiniteViewMedia`` is returned.
You will have to provide the appropriate ``InfiniteMediaData`` when you provide ``InfiniteItemViewData`` during startup.
You can is it in your widget tree as in following example.

```dart
builderWithMedia: (context, value, index, media) => Container(
    color: value.color,
    child: Stack(
      children: [
        media.isNotEmpty ? media.first : Container(),
        Align(
          alignment: Alignment.bottomCenter,
          child: Text(value.text),
        ),
      ],
    ),
  ),
```

Use ``onRefresh`` for custom callback when user pulls the list down.
Use ``colorRefreshIndicator`` for custom color for indicator when user pulls the list down.
Use ``noMoreItemsIndicatorBuilder`` for custom view when user scrolled to the end.
Use ``noFoundItemsIndicatorBuilder`` for custom view when no items were presented.
## Additional information

This package is optimized for a long list of lots of media. 
Especially videos require memory of your device.
If you still suffer please add these lines to your ``Ìnfo.plist``.

```dart
<key>NSAppTransportSecurity</key>
    	<dict>
    		<key>NSAllowsArbitraryLoads</key>
    		<true/>
    		<key>NSAllowsArbitraryLoadsForMedia</key>
    		<true/>
    		<key>NSAllowsLocalNetworking</key>
    		<true/>
    	</dict>
```

## Future releases
In future releases you will be able:
- a customizable switch for views 
- view of map and albums