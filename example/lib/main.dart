import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:infinite/infinite.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Infinite Demo',
      theme: ThemeData.dark(),
      themeMode: ThemeMode.dark,
      home: const MyHomePage(title: 'Infinite widget'),
    );
  }
}

/// Data that is later passed to [InfiniteItemViewData].
class MyListData {
  MyListData({
    required this.color,
    required this.text,
  });

  final String text;
  final Color color;
}

/// Simple home page with title.
class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

/// State that holds example items.
///
/// In your usage please use Bloc or other state management to provide the data.
class _MyHomePageState extends State<MyHomePage> {
  final _keyInfinite = GlobalKey<InfiniteViewState>();

  List<InfiniteItemViewData<MyListData>> items = [];

  /// Creates example items.
  @override
  void initState() {
    items = _onCreateItems();
    super.initState();
  }

  /// In [Scaffold] body we provide [Infinite.infiniteView]
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Infinite.infiniteView<MyListData, InfiniteItemViewData<MyListData>>(
        key: _keyInfinite,
        itemsCount: items.length,
        mode: InfiniteViewMode.list,
        delegateList: InfiniteListDelegate(
          maxWidth: size.width,
          builder: (context, value, index) => Container(),
          builderWithMedia: (context, value, index, media) => Container(
            constraints: const BoxConstraints(minHeight: 100),
            color: value.color,
            child: Column(
              children: [
                media.isNotEmpty ? media.first : Container(),
                SizedBox(height: 100, child: Center(child: Text(value.text))),
              ],
            ),
          ),
          itemExtent: 100,
          pageSize: 7,
        ),
        delegateGrid: InfiniteGridDelegate(
          maxWidth: size.width,
          builderWithMedia: (context, value, index, media) => Container(
            color: value.color,
            child: Stack(
              children: [
                media.isNotEmpty ? media.first : Container(),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Text(value.text),
                ),
              ],
            ),
          ),
          itemExtent: size.width / 2,
          columnsCount: 3,
          pageSize: 20,
        ),
        getData: _onGetData,
        onRefresh: _onRefresh,
      ),
      floatingActionButton: FloatingActionButton(onPressed: _onSwitchMode),
    );
  }

  List<InfiniteItemViewData<MyListData>> _onCreateItems() {
    return List.generate(
      20000,
      (index) => InfiniteItemViewData(
        value: MyListData(
          text: 'Hi$index',
          color: _randomColor(),
        ),
        media: [
          InfiniteMediaData(
            id: index.toString(),
            url:
                'https://storage.googleapis.com/cms-storage-bucket/6f183a9db312d0e1b535.png',
            isVideo: false,
          ),
        ],
      ),
    );
  }

  Future<void> _onRefresh() async {
    await Future(() => items = _onCreateItems());

    _keyInfinite.currentState?.clear();

    setState(() {});
  }

  Future<List<InfiniteItemViewData<MyListData>>> _onGetData(
    startIndex,
    nextItemsCount,
  ) async {
    if (startIndex < 0) return [];

    int endIndex = startIndex + nextItemsCount + 1;
    endIndex = endIndex < items.length ? endIndex : items.length;

    return await Future(() => items.sublist(startIndex, endIndex));
  }

  _onSwitchMode() {
    final selectedMode = _keyInfinite.currentState?.mode;

    if (selectedMode != null) {
      switch (selectedMode) {
        case InfiniteViewMode.list:
          _keyInfinite.currentState?.onReloadMode(InfiniteViewMode.grid);
          break;
        case InfiniteViewMode.grid:
          _keyInfinite.currentState?.onReloadMode(InfiniteViewMode.list);
          break;
      }
    }
  }

  Color _randomColor() {
    return Color((math.Random().nextDouble() * 0xFFFFFF).toInt())
        .withOpacity(1.0);
  }
}
